Role Name
=========

The goobi-viewer is a Ansible role which can be used to automatically set up a Goobi viewer on a server.
The role is developed based on the official Goobi viewer installation [document](https://docs.goobi.io/goobi-viewer-en/devs-and-ops/3).

Requirements
------------

The goobi-viewer role currently works by setting up Goobi viewer on an existing Ubuntu 20.04 server (eg. NREC servers), which means the Ubuntu setup is required before using the goobi-viewer role. See Ansible role [Ubuntu](https://git.app.uib.no/uib-ub/drift/roller-ansible/ubuntu) for setting up Ubuntu. 

Role Variables
--------------

Most of variables for Goobi viewer setup are defined in defaults/main, however, the environment variables are defined in vars/main. For other needed variables, please check the usage in [ansible-goobi](https://git.app.uib.no/samla/ansible-goobi) repo.


Dependencies
------------

Ansible role [Ubuntu](https://git.app.uib.no/uib-ub/drift/roller-ansible/ubuntu).

Example Playbook
----------------

    - name: "import role goobi-viewer"
      import_role:
        name: "goobi-viewer"

See [goobi-wiewer playbook](https://git.app.uib.no/samla/ansible-goobi/-/blob/master/goobi-viewer.yml).

License
-------

BSD

Author Information
------------------

Rui Wang, University of Bergen
